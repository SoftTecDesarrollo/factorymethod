﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    class PostgreSQL : ConnectionStrings
    {
        public override string GetConnectionStrings()
        {
            return "User ID = root; Password = myPassword; Host = localhost; Port = 5432; Database = myDataBase; Pooling = true; Min Pool Size = 0; Max Pool Size = 100; Connection Lifetime = 0;";
        }
    }
}