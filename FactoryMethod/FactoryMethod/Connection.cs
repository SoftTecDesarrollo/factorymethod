﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    public class Connection
    {
        public const string POSTGRESQL = "POSTGRESQL";
        public const string SQLSERVER = "SQLSERVER";

        public static ConnectionStrings ConnectionString(string tipo)
        {
            switch (tipo)
            {
                case POSTGRESQL:
                    return new PostgreSQL();
                case SQLSERVER:
                    return new SQLServer();
                default: return null;
            }
        }
    }
}
