﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    class SQLServer : ConnectionStrings
    {
        public override string GetConnectionStrings()
        {
            return "Server = myServerAddress; Database = myDataBase; User Id = myUsername; Password = myPassword;";
        }
    }
}